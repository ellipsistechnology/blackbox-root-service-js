"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var blackbox_services_1 = require("blackbox-services");
var RootService = /** @class */ (function () {
    function RootService() {
    }
    RootService.prototype.getRootService = function (_a) {
        _a = {};
        return blackbox_services_1.makeServiceObject(this.oasDoc, '');
    };
    __decorate([
        blackbox_ioc_1.autowired('oasDoc')
    ], RootService.prototype, "oasDoc", void 0);
    RootService = __decorate([
        blackbox_ioc_1.serviceClass('root-service')
    ], RootService);
    return RootService;
}());
exports.RootService = RootService;
var ParentServiceCreator = /** @class */ (function () {
    function ParentServiceCreator() {
    }
    ParentServiceCreator.prototype.create = function () {
        var _this = this;
        var parentServices = blackbox_services_1.findParentPaths(this.oasDoc);
        parentServices.forEach(function (servicePath) {
            var serviceName = servicePath.startsWith('/') ? servicePath.substring(1) : servicePath; // strip preceding slash
            var tag = serviceName + "-service";
            var operationId = _this.oasDoc.paths[servicePath].get.operationId;
            // Create service function:
            var s = blackbox_services_1.makeServiceObject(_this.oasDoc, serviceName);
            if (!_this[operationId]) {
                _this[operationId] = function () { return s; };
                // Tag service function:
                blackbox_ioc_1.taggedService(tag, operationId)(_this, operationId, undefined);
            }
        });
    };
    __decorate([
        blackbox_ioc_1.autowired('oasDoc')
    ], ParentServiceCreator.prototype, "oasDoc", void 0);
    return ParentServiceCreator;
}());
function init() {
    new ParentServiceCreator().create();
}
exports.default = init;
