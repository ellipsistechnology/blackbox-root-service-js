"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var blackbox_ioc_1 = require("blackbox-ioc");
var __1 = require("..");
var fs = require("fs");
var path = require("path");
new __1.RootService(); // force loading of services
var _TestSetup = /** @class */ (function () {
    function _TestSetup() {
    }
    _TestSetup.prototype.loadOasDoc = function () {
        var oasDocData = fs.readFileSync(path.join(__dirname, 'openapi.json'));
        return JSON.parse(oasDocData.toString());
    };
    __decorate([
        blackbox_ioc_1.factory('oasDoc')
    ], _TestSetup.prototype, "loadOasDoc", null);
    return _TestSetup;
}());
var Test = /** @class */ (function () {
    function Test() {
    }
    __decorate([
        blackbox_ioc_1.autowiredService('root-service')
    ], Test.prototype, "rootService", void 0);
    __decorate([
        blackbox_ioc_1.autowired('oasDoc')
    ], Test.prototype, "oasDoc", void 0);
    return Test;
}());
test("Root service provides getRootService method.", function () {
    var test = new Test();
    var service = test.rootService;
    expect(service).toBeDefined();
    expect(service.getRootService).toBeDefined();
    expect(service.getRootService()).toEqual({
        description: test.oasDoc.paths['/'].get.summary,
        bbversion: "0.0.1",
        links: {
            self: { href: "/" },
            rulebase: {
                href: "/rulebase",
                description: test.oasDoc.paths['/rulebase'].get.summary,
                types: [{
                        uri: "http://ellipsistechnology.com/schemas/blackbox/",
                        name: "rule"
                    },
                    {
                        uri: "http://ellipsistechnology.com/schemas/blackbox/",
                        name: "condition"
                    },
                    {
                        uri: "http://ellipsistechnology.com/schemas/blackbox/",
                        name: "value"
                    }]
            },
            weather: {
                href: "/weather",
                description: test.oasDoc.paths['/weather'].get.summary,
                types: [{
                        uri: "http://ellipsistechnology.com/schemas/blackbox/",
                        name: "temperature"
                    }]
            }
        }
    });
});
test("Parent services are created with init().", function () {
    var ServiceWrapper = /** @class */ (function () {
        function ServiceWrapper() {
        }
        __decorate([
            blackbox_ioc_1.autowiredService('weather-service')
        ], ServiceWrapper.prototype, "testWeatherService", void 0);
        __decorate([
            blackbox_ioc_1.autowired('oasDoc')
        ], ServiceWrapper.prototype, "oasDoc", void 0);
        return ServiceWrapper;
    }());
    __1.default();
    var wrapper = new ServiceWrapper();
    expect(wrapper.testWeatherService).toBeDefined();
    expect(wrapper.testWeatherService.getWeather).toBeDefined();
    expect(wrapper.testWeatherService.getWeather()).toEqual({
        description: wrapper.oasDoc.paths['/weather'].get.summary,
        bbversion: "0.0.1",
        links: {
            self: { href: "/weather" },
            temperature: {
                href: "/weather/temperature",
                description: wrapper.oasDoc.paths['/weather/temperature'].get.summary,
                types: [{
                        uri: "http://ellipsistechnology.com/schemas/blackbox/",
                        name: "temperature"
                    }]
            },
            humidity: {
                href: "/weather/humidity",
                description: wrapper.oasDoc.paths['/weather/humidity'].get.summary,
                types: []
            }
        }
    });
});
