import { Service } from 'blackbox-services';
export declare class RootService {
    oasDoc: any;
    getRootService({}?: any): Service;
}
export default function init(): void;
