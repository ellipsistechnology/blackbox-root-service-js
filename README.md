# blackbox-root-service

[![Version](https://img.shields.io/bitbucket/pipelines/ellipsistechnology/blackbox-root-service-js.svg)](https://bitbucket.org/ellipsistechnology/blackbox-root-service-js/addon/pipelines/home#!/)
[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg)](https://github.com/RichardLitt/standard-readme)

> Automatically creates services for the Blackbox API root and any parent services.

Through the `init()` function, services are automatically created for the Blackbox API root and any parent services. The services are added to the [Blackbox IOC](https://www.npmjs.com/package/blackbox-ioc) container for use by a Blackbox server (typically generated via the [Blackbox CLI](https://www.npmjs.com/package/blackbox-cli)).

For further information about the Blackbox Specification refer to the Blackbox website.

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [API](#api)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```
npm i blackbox-root-service
```

## Usage

Generate service handlers for all parent services:
```
import init from "blackbox-root-service"
init()
```
The generated service handlers will be tagged as services in the [Blackbox IOC](https://www.npmjs.com/package/blackbox-ioc) container. They can then be accessed from server to provide the response for a request to the parent service API.

The generated services rely on the OpenAPI document, an instance of which must be provided to the IOC container and named 'oasDoc', for example:
```
@factory('oasDoc')
loadOasDoc() {
  const oasDocData:any = fs.readFileSync(path.join(__dirname,'openapi.json'))
  return JSON.parse(oasDocData.toString())
}
```

Generally the above two steps will be all you will want to do - for example, the OpenAPI document is named via a factory and `init()` is called from index.ts in a Blackbox server generated with the [Blackbox CLI](https://www.npmjs.com/package/blackbox-cli). However, sometimes you may wish to access the root service or a child service. This is easily done through autowiring:
```
class MyClass {
  @autowiredService('root-service')
  rootService:any

  @autowiredService('my-service')
  myService:any
}
```

## Maintainers

[@ellipsistechnology](https://github.com/ellipsistechnology)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2019 Ben Millar
