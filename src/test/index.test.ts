import { autowiredService, autowired, factory } from "blackbox-ioc";
import init, { RootService } from "..";
import fs = require('fs')
import path = require('path');

new RootService() // force loading of services

class _TestSetup {
  @factory('oasDoc')
  loadOasDoc() {
    const oasDocData:any = fs.readFileSync(path.join(__dirname,'openapi.json'))
    return JSON.parse(oasDocData.toString())
  }
}

class Test {
  @autowiredService('root-service')
  rootService:any

  @autowired('oasDoc')
  oasDoc:any
}

test("Root service provides getRootService method.", () => {
  const test = new Test()
  const service = test.rootService

  expect(service).toBeDefined()
  expect(service.getRootService).toBeDefined()
  expect(service.getRootService()).toEqual({
    description: test.oasDoc.paths['/'].get.summary,
    bbversion: "0.0.1",
    links: {
      self: { href: "/" },
      rulebase: {
        href: "/rulebase",
        description: test.oasDoc.paths['/rulebase'].get.summary,
        types: [{
          uri:"http://ellipsistechnology.com/schemas/blackbox/",
          name:"rule"
        },
        {
          uri:"http://ellipsistechnology.com/schemas/blackbox/",
          name:"condition"
        },
        {
          uri:"http://ellipsistechnology.com/schemas/blackbox/",
          name:"value"
        }]
      },
      weather: {
        href: "/weather",
        description: test.oasDoc.paths['/weather'].get.summary,
        types: [{
          uri:"http://ellipsistechnology.com/schemas/blackbox/",
          name:"temperature"
        }]
      }
    }
  })
})

test("Parent services are created with init().", () => {
  class ServiceWrapper {
    @autowiredService('weather-service')
    testWeatherService: any

    @autowired('oasDoc')
    oasDoc:any
  }
  init()

  const wrapper = new ServiceWrapper()
  expect(wrapper.testWeatherService).toBeDefined()

  expect(wrapper.testWeatherService.getWeather).toBeDefined()
  expect(wrapper.testWeatherService.getWeather()).toEqual({
    description: wrapper.oasDoc.paths['/weather'].get.summary,
    bbversion: "0.0.1",
    links: {
      self: { href: "/weather" },
      temperature: {
        href: "/weather/temperature",
        description: wrapper.oasDoc.paths['/weather/temperature'].get.summary,
        types: [{
          uri:"http://ellipsistechnology.com/schemas/blackbox/",
          name:"temperature"
        }]
      },
      humidity: {
        href: "/weather/humidity",
        description: wrapper.oasDoc.paths['/weather/humidity'].get.summary,
        types: []
      }
    }
  })
})
