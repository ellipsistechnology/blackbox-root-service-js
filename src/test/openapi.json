{
  "openapi": "3.0.1",
  "info": {
    "title": "A weather API.",
    "version": "0.0.1"
  },
  "components": {
    "parameters": {
      "path-name": {
        "in": "path",
        "name": "name",
        "schema": {
          "type": "string"
        },
        "required": true,
        "description": "The object's name."
      },
      "query-verbose": {
        "in": "query",
        "name": "verbose",
        "schema": {
          "type": "string"
        },
        "required": false,
        "description": "Indicates that the response must have verbose output."
      },
      "query-depth": {
        "in": "query",
        "name": "depth",
        "schema": {
          "type": "number"
        },
        "required": false,
        "description": "Indicates the depth of the returned object's hierarchy."
      }
    },
    "schemas": {
      "link": {
        "type": "object",
        "required": [
          "href"
        ],
        "properties": {
          "href": {
            "type": "string"
          },
          "description": {
            "type": "string"
          }
        }
      },
      "links": {
        "type": "object",
        "required": [
          "self"
        ],
        "properties": {
          "self": {
            "$ref": "#/components/schemas/link"
          }
        },
        "additionalProperties": {
          "$ref": "#/components/schemas/link"
        }
      },
      "verbose-object": {
        "type": "object",
        "properties": {
          "links": {
            "$ref": "#/components/schemas/links"
          }
        }
      },
      "service": {
        "type": "object",
        "required": [
          "description",
          "links",
          "bbversion"
        ],
        "properties": {
          "description": {
            "type": "string"
          },
          "bbversion": {
            "type": "string"
          },
          "links": {
            "type": "object",
            "required": [
              "self"
            ],
            "properties": {
              "self": {
                "$ref": "#/components/schemas/link"
              }
            },
            "additionalProperties": {
              "allOf": [
                {
                  "$ref": "#/components/schemas/link"
                },
                {
                  "type": "object",
                  "required": [
                    "types"
                  ],
                  "properties": {
                    "types": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "required": [
                          "uri"
                        ],
                        "properties": {
                          "uri": {
                            "type": "string"
                          },
                          "format": {
                            "type": "string"
                          },
                          "description": {
                            "type": "string"
                          }
                        }
                      }
                    }
                  }
                }
              ]
            }
          }
        }
      },
      "rule": {
        "type": "object",
        "required": [
          "name",
          "trigger",
          "action"
        ],
        "properties": {
          "name": {
            "type": "string"
          },
          "preCondition": {
            "oneOf": [
              {
                "$ref": "#/components/schemas/condition"
              },
              {
                "$ref": "#/components/schemas/link"
              }
            ]
          },
          "postCondition": {
            "oneOf": [
              {
                "$ref": "#/components/schemas/condition"
              },
              {
                "$ref": "#/components/schemas/link"
              }
            ]
          },
          "trigger": {
            "oneOf": [
              {
                "$ref": "#/components/schemas/condition"
              },
              {
                "$ref": "#/components/schemas/link"
              }
            ]
          },
          "action": {
            "oneOf": [
              {
                "$ref": "#/components/schemas/condition"
              },
              {
                "$ref": "#/components/schemas/link"
              }
            ]
          }
        }
      },
      "condition": {
        "type": "object",
        "required": [
          "name",
          "left",
          "right",
          "type"
        ],
        "properties": {
          "name": {
            "type": "string"
          },
          "left": {
            "oneOf": [
              {
                "$ref": "#/components/schemas/value"
              },
              {
                "$ref": "#/components/schemas/link"
              }
            ]
          },
          "right": {
            "oneOf": [
              {
                "$ref": "#/components/schemas/value"
              },
              {
                "$ref": "#/components/schemas/link"
              }
            ]
          },
          "type": {
            "$ref": "#/components/schemas/condition-type"
          }
        }
      },
      "condition-type": {
        "type": "string",
        "enum": [
          "=",
          ">",
          "<",
          "and",
          "or"
        ]
      },
      "value": {
        "type": "object",
        "required": [
          "type",
          "value"
        ],
        "properties": {
          "type": {
            "type": "string"
          },
          "value": {
            "oneOf": [
              {
                "type": "string"
              },
              {
                "type": "object"
              }
            ]
          }
        }
      },
      "named-reference": {
        "type": "object",
        "required": [
          "name"
        ],
        "properties": {
          "name": {
            "type": "string"
          }
        }
      },
      "temperature": {
        "type": "object",
        "properties": {
          "temp": {
            "type": "number"
          },
          "scale": {
            "type": "string"
          }
        }
      }
    }
  },
  "paths": {
    "/": {
      "get": {
        "summary": "A list of services provided by this API.",
        "operationId": "getRootService",
        "tags": [
          "root"
        ],
        "responses": {
          "200": {
            "description": "200 OK Response: The list of services.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "required": [
                    "rulebase",
                    "weather"
                  ],
                  "properties": {
                    "rulebase": {
                      "$ref": "#/components/schemas/service"
                    },
                    "weather": {
                      "$ref": "#/components/schemas/service"
                    }
                  },
                  "additionalProperties": {
                    "$ref": "#/components/schemas/service"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/rulebase": {
      "get": {
        "summary": "The blackbox rulebase service.",
        "operationId": "getRulebaseService",
        "tags": [
          "rulebase"
        ],
        "responses": {
          "200": {
            "description": "200 OK Response: The list of rulebase services.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "required": [
                    "rules",
                    "conditions",
                    "values"
                  ],
                  "properties": {
                    "rules": {
                      "$ref": "#/components/schemas/service"
                    },
                    "conditions": {
                      "$ref": "#/components/schemas/service"
                    },
                    "values": {
                      "$ref": "#/components/schemas/service"
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/rulebase/rules": {
      "x-blackbox-types": [{"uri": "http://ellipsistechnology.com/schemas/blackbox/", "name": "rule"}],
      "get": {
        "summary": "Provides a list of black box rules.",
        "operationId": "getRules",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/query-verbose"
          },
          {
            "$ref": "#/components/parameters/query-depth"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response: The list of rules.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "anyOf": [
                      {
                        "$ref": "#/components/schemas/rule"
                      },
                      {
                        "$ref": "#/components/schemas/verbose-object"
                      }
                    ]
                  }
                }
              }
            }
          }
        }
      },
      "post": {
        "summary": "Creates a rule.",
        "operationId": "createRule",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/query-verbose"
          }
        ],
        "requestBody": {
          "description": "A blackbox rule.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/rule"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "200 OK Response: Successful creation. The rule name will be returned. Supports a verbose response.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/named-reference"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      }
    },
    "/rulebase/rules/{name}": {
      "get": {
        "summary": "Request a rule identified by its name.",
        "operationId": "getRule",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          },
          {
            "$ref": "#/components/parameters/query-depth"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response: The requested rule.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/rule"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "put": {
        "summary": "Replace the rule identified by its name.",
        "operationId": "replaceRule",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          }
        ],
        "requestBody": {
          "description": "A Blackbox rule.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/rule"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "200 OK Response: The rule name will be returned. Supports a verbose response.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/named-reference"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "patch": {
        "summary": "Change some of the fields of the rule identified by its name.",
        "operationId": "updateRule",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          }
        ],
        "requestBody": {
          "description": "A Blackbox rule.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/rule"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "200 OK Response: The rule name will be returned. Supports a verbose response.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/named-reference"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "delete": {
        "summary": "Delete the rule identified by its name.",
        "operationId": "deleteRule",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response: The rule name will be returned.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/named-reference"
                }
              }
            }
          }
        }
      }
    },
    "/rulebase/conditions": {
      "x-blackbox-types": [{"uri": "http://ellipsistechnology.com/schemas/blackbox/", "name": "condition"}],
      "get": {
        "summary": "Provides a list of black box conditions.",
        "operationId": "getConditions",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/query-verbose"
          },
          {
            "$ref": "#/components/parameters/query-depth"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response: The list of conditions.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "anyOf": [
                      {
                        "$ref": "#/components/schemas/condition"
                      },
                      {
                        "$ref": "#/components/schemas/verbose-object"
                      }
                    ]
                  }
                }
              }
            }
          }
        }
      },
      "post": {
        "summary": "Creates a condition.",
        "operationId": "createCondition",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/query-verbose"
          }
        ],
        "requestBody": {
          "description": "A blackbox condition.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/condition"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "200 OK Response: Successful creation. The condition name will be returned. Supports a verbose response.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/named-reference"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      }
    },
    "/rulebase/conditions/{name}": {
      "get": {
        "summary": "Request a condition identified by its name.",
        "operationId": "getCondition",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          },
          {
            "$ref": "#/components/parameters/query-depth"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response: The requested condition.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/condition"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "put": {
        "summary": "Replace the condition identified by its name.",
        "operationId": "replaceCondition",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          }
        ],
        "requestBody": {
          "description": "A Blackbox condition.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/condition"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "200 OK Response: The condition name will be returned. Supports a verbose response.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/named-reference"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "patch": {
        "summary": "Change some of the fields of the condition identified by its name.",
        "operationId": "updateCondition",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          }
        ],
        "requestBody": {
          "description": "A Blackbox condition.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/condition"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "200 OK Response: The condition name will be returned. Supports a verbose response.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/named-reference"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "delete": {
        "summary": "Delete the condition identified by its name.",
        "operationId": "deleteCondition",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response: The condition name will be returned.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/named-reference"
                }
              }
            }
          }
        }
      }
    },
    "/rulebase/values": {
      "x-blackbox-types": [{"uri": "http://ellipsistechnology.com/schemas/blackbox/", "name": "value"}],
      "get": {
        "summary": "Provides a list of black box values.",
        "operationId": "getValues",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/query-verbose"
          },
          {
            "$ref": "#/components/parameters/query-depth"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response: The list of values.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "anyOf": [
                      {
                        "$ref": "#/components/schemas/value"
                      },
                      {
                        "$ref": "#/components/schemas/verbose-object"
                      }
                    ]
                  }
                }
              }
            }
          }
        }
      },
      "post": {
        "summary": "Creates a value.",
        "operationId": "createValue",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/query-verbose"
          }
        ],
        "requestBody": {
          "description": "A blackbox value.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/value"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "200 OK Response: Successful creation. The value name will be returned. Supports a verbose response.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/named-reference"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      }
    },
    "/rulebase/values/{name}": {
      "get": {
        "summary": "Request a value identified by its name.",
        "operationId": "getValue",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          },
          {
            "$ref": "#/components/parameters/query-depth"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response: The requested value.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/value"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "put": {
        "summary": "Replace the value identified by its name.",
        "operationId": "replaceValue",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          }
        ],
        "requestBody": {
          "description": "A Blackbox value.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/value"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "200 OK Response: The value name will be returned. Supports a verbose response.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/named-reference"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "patch": {
        "summary": "Change some of the fields of the value identified by its name.",
        "operationId": "updateValue",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          }
        ],
        "requestBody": {
          "description": "A Blackbox value.",
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/value"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "200 OK Response: The value name will be returned. Supports a verbose response.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/named-reference"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      },
      "delete": {
        "summary": "Delete the value identified by its name.",
        "operationId": "deleteValue",
        "tags": [
          "rulebase"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response: The value name will be returned.",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/named-reference"
                }
              }
            }
          }
        }
      }
    },
    "/weather": {
      "get": {
        "summary": "Weather service: Provides temperature and humidity information.",
        "operationId": "getWeather",
        "tags": [
          "weather"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/query-depth"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "object",
                  "required": [
                    "temperature",
                    "humidity"
                  ],
                  "properties": {
                    "temperature": {
                      "$ref": "#/components/schemas/service"
                    },
                    "humidity": {
                      "$ref": "#/components/schemas/service"
                    }
                  }
                }
              }
            }
          }
        }
      }
    },
    "/weather/temperature": {
      "x-blackbox-types": [{"uri": "http://ellipsistechnology.com/schemas/blackbox/", "name": "temperature"}],
      "get": {
        "summary": "Returns a temperature object.",
        "operationId": "getTemperatures",
        "tags": [
          "weather"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/query-depth"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "allOf": [
                      {
                        "$ref": "#/components/schemas/temperature"
                      },
                      {
                        "$ref": "#/components/schemas/verbose-object"
                      }
                    ]
                  }
                }
              }
            }
          }
        }
      }
    },
    "/weather/temperature/{name}": {
      "get": {
        "summary": "Gets a temperature by name.",
        "operationId": "getTemperature",
        "tags": [
          "weather",
          "weather"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          },
          {
            "$ref": "#/components/parameters/query-depth"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response.",
            "content": {
              "application/json": {
                "schema": {
                  "allOf": [
                    {
                      "$ref": "#/components/schemas/temperature"
                    },
                    {
                      "$ref": "#/components/schemas/verbose-object"
                    }
                  ]
                }
              }
            }
          }
        }
      }
    },
    "/weather/humidity": {
      "get": {
        "summary": "Retrieve a list of humidity objects.",
        "operationId": "getHumidities",
        "tags": [
          "weather"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/query-depth"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "type": "number"
                  }
                }
              }
            }
          }
        }
      }
    },
    "/weather/humidity/{name}": {
      "get": {
        "summary": "Gets a humidity by name.",
        "operationId": "getHumidity",
        "tags": [
          "weather",
          "weather"
        ],
        "parameters": [
          {
            "$ref": "#/components/parameters/path-name"
          },
          {
            "$ref": "#/components/parameters/query-verbose"
          },
          {
            "$ref": "#/components/parameters/query-depth"
          }
        ],
        "responses": {
          "200": {
            "description": "200 OK Response.",
            "content": {
              "application/json": {
                "schema": {
                  "type": "number"
                }
              }
            }
          }
        }
      }
    }
  }
}
