import {serviceClass, autowired, taggedService} from 'blackbox-ioc'
import {Service, makeServiceObject, findParentPaths} from 'blackbox-services'

@serviceClass('root-service')
export class RootService {
  @autowired('oasDoc')
  oasDoc: any

  getRootService({}:any={}):Service {
    return makeServiceObject(this.oasDoc, '')
  }
}

class ParentServiceCreator {
  [key:string]:Function

  @autowired('oasDoc')
  oasDoc:any

  create() {
    const parentServices = findParentPaths(this.oasDoc)

    parentServices.forEach((servicePath:string) => {
      const serviceName = servicePath.startsWith('/') ? servicePath.substring(1) : servicePath // strip preceding slash
      const tag = serviceName+"-service"
      const operationId = this.oasDoc.paths[servicePath].get.operationId

      // Create service function:
      const s = makeServiceObject(this.oasDoc, serviceName)
      if(!this[operationId]) {
        this[operationId] = () => s

        // Tag service function:
        taggedService(tag, operationId)(this, operationId, undefined)
      }
    })
  }
}

export default function init() {
  new ParentServiceCreator().create()
}
